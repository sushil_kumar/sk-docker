from rest_framework.routers import DefaultRouter
from . views import infoview
from . import views
from django.urls import path,include

router=DefaultRouter()
router.register('',infoview,basename='sk')

urlpatterns=[
    path('api/',include(router.urls)),
    path('',views.index),
    path('show',views.show),
]
