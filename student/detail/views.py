from django.shortcuts import render
from django.http import HttpResponse
from  rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import status
from .models import info
from .srz import infosrz



class infoviews(viewsets.ViewSet):
    def list(self,req):
        record=info.objects.all()
        serializer=infosrz(record,many=True)
        return Response(serializer.data)

    def retrieve(self,req,pk=None):
        id=pk
        if id is not None:
            record=info.objects.get(id=id)
            serializer=infosrz(record)
            return Response(serializer.data)

    def create(self,req):
        serializer=infosrz(data=req.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg':'Data created'},status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

    def update(self,req,pk):
        id=pk
        record=info.objects.get(pk=id)
        serializer=infosrz(record,data=req.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg':'complete data updated'})
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self,req,pk):
        id=pk
        record=info.objects.get(pk=id)
        serializer=infosrz(record,data=req.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg':'partial data updated'})
        return Response(serializer.errors)

    def destroy(self,req,pk):
        id=pk
        record=info.objects.get(pk=id)
        record.delete()
        return Response({'msg':'one record deleted'})








def index(req):
    return render(req, 'index.html' )

def show(req):
    d=info.objects.all()
    return render(req,'show.html',{'data':d})


class infoview(viewsets.ModelViewSet):
    serializer_class=infosrz
    queryset=info.objects.all()
