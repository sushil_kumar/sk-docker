from rest_framework.serializers import ModelSerializer
from .models import info


class infosrz(ModelSerializer):
    class Meta:
        model=info
        fields='__all__'
