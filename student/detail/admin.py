from django.contrib import admin
from detail.models import info

# Register your models here.

class myinfo(admin.ModelAdmin):
    list_display=('id','name','father','email','phone')

admin.site.register(info,myinfo)
